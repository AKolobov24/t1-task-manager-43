package ru.t1.akolobov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataXmlSaveJaxBRequest extends AbstractUserRequest {

    public DataXmlSaveJaxBRequest(@Nullable String token) {
        super(token);
    }

}
