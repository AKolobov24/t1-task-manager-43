package ru.t1.akolobov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public final class SessionDto extends AbstractUserOwnedDtoModel {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private Date date = new Date();

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof SessionDto) {
            SessionDto anotherSession = (SessionDto) obj;
            return this.getId().equals(anotherSession.getId()) &&
                    this.getUserId().equals(anotherSession.getUserId()) &&
                    this.getRole() == anotherSession.getRole();
        }
        return false;
    }

}
