package ru.t1.akolobov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Status;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class ProjectDto extends AbstractUserOwnedDtoModel {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    public ProjectDto(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDto(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDto(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description + "(" + getId() + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof ProjectDto) {
            ProjectDto anotherProject = (ProjectDto) obj;
            return this.getId().equals(anotherProject.getId()) &&
                    this.getName().equals(anotherProject.getName()) &&
                    this.getStatus() == anotherProject.getStatus();
        }
        return false;
    }


}
