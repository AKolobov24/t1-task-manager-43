package ru.t1.akolobov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.*;
import ru.t1.akolobov.tm.api.service.*;
import ru.t1.akolobov.tm.api.service.dto.*;
import ru.t1.akolobov.tm.endpoint.*;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.service.*;
import ru.t1.akolobov.tm.service.dto.*;
import ru.t1.akolobov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @Getter
    @NotNull
    private final ITaskDtoService taskService = new TaskDtoService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskDtoService projectTaskService = new ProjectTaskDtoService(connectionService);

    @Getter
    @NotNull
    private final IUserDtoService userService = new UserDtoService(
            connectionService,
            propertyService
    );

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(
            projectService,
            taskService,
            userService
    );

    @NotNull
    private final Backup backup = new Backup(domainService);

    @Getter
    @NotNull
    private final ISessionDtoService sessionService = new SessionDtoService(connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        try {
            @NotNull final String user1Id = Optional
                    .ofNullable(userService.findByLogin("user1"))
                    .orElse(
                            userService.create(
                                    "user1",
                                    "user1",
                                    "user@mail.ru"
                            )
                    ).getId();
            @NotNull final String user2Id = Optional
                    .ofNullable(userService.findByLogin("akolobov"))
                    .orElse(
                            userService.create(
                                    "akolobov",
                                    "akolobov",
                                    Role.ADMIN)
                    ).getId();
            projectService.create(user1Id, "TEST PROJECT", Status.IN_PROGRESS);
            projectService.create(user1Id, "DEMO PROJECT", Status.COMPLETED);
            projectService.create(user2Id, "BETA PROJECT", Status.NOT_STARTED);
            projectService.create(user2Id, "BEST PROJECT", Status.IN_PROGRESS);
            taskService.create(user1Id, "TEST TASK", Status.IN_PROGRESS);
            taskService.create(user1Id, "DEMO TASK", Status.COMPLETED);
            taskService.create(user1Id, "BETA TASK", Status.NOT_STARTED);
            taskService.create(user1Id, "BEST TASK", Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            System.out.println("Error while initiating demo data!");
            System.err.println("\t" + e.getMessage());
        }
    }

    public void run() {
        initPid();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        backup.stop();
    }

}
