package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.akolobov.tm.model.AbstractUserOwnedModel;
import ru.t1.akolobov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUser(entityManager.find(User.class, userId));
        add(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUser(entityManager.find(User.class, userId));
        update(model);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        removeById(userId, model.getId());
    }

    private void removeAll(final @NotNull List<M> models) {
        if (models.isEmpty()) return;
        models.forEach(this::remove);
    }

}
