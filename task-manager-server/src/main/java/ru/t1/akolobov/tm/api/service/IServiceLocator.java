package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    IProjectTaskDtoService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IUserDtoService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDtoService getSessionService();

}
