package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.model.IUserRepository;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull String jpql = "DELETE FROM User";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull String id) {
        return entityManager.find(User.class, id);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull String jpql = "SELECT u FROM User u";
        return entityManager.createQuery(jpql, User.class)
                .getResultList();
    }

    @Override
    public @NotNull List<User> findAll(@NotNull Sort sort) {
        @NotNull String jpql = "SELECT u FROM User u ORDER BY u." + sort.getColumnName();
        return entityManager.createQuery(jpql, User.class)
                .getResultList();
    }

    @Override
    public Long getSize() {
        @NotNull String jpql = "SELECT COUNT(u) FROM User t";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        User user = entityManager.find(User.class, id);
        if (user == null) return;
        entityManager.remove(user);
    }


    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull String jpql = "SELECT u FROM User WHERE u.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull String jpql = "SELECT u FROM User WHERE u.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
