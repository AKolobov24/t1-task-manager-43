package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.model.IProjectRepository;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull String jpql = "DELETE FROM Project";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String id) {
        return entityManager.find(Project.class, id);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        @NotNull String jpql = "SELECT p FROM Project p";
        return entityManager.createQuery(jpql, Project.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull Sort sort) {
        @NotNull String jpql = "SELECT p FROM Project p ORDER BY p." + sort.getColumnName();
        return entityManager.createQuery(jpql, Project.class)
                .getResultList();
    }

    @Override
    public Long getSize() {
        @NotNull String jpql = "SELECT COUNT(p) FROM Project p";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        Project project = entityManager.find(Project.class, id);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull String jpql = "DELETE FROM Project p WHERE p.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId) {
        @NotNull String jpql = "SELECT p FROM Project p WHERE p.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId, @NotNull Sort sort) {
        @NotNull String jpql = "SELECT p FROM Project p WHERE p.user.id = :userId " +
                "ORDER BY p." + sort.getColumnName();
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull String jpql = "SELECT p FROM Project p " +
                "WHERE p.id = :id AND p.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize(@NotNull String userId) {
        @NotNull String jpql = "SELECT COUNT(p) FROM Project p WHERE p.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        Project project = findOneById(userId, id);
        if (project == null) return;
        entityManager.remove(project);
    }

}
