package ru.t1.akolobov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.model.Session;

import java.util.Date;

public interface ISessionService extends IUserOwnedService<Session> {

    @NotNull
    Session updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable Date date,
                       @Nullable Role role);

}
