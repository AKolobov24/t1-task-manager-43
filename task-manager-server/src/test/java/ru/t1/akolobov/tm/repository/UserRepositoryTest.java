package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.repository.dto.UserDtoRepository;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    private static EntityManager entityManager;

    @BeforeClass
    public static void prepareConnection() {
        entityManager = new ConnectionService(new PropertyService()).getEntityManager();
    }

    @AfterClass
    public static void closeConnection() {
        entityManager.close();
    }

    @Before
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @Test
    public void add() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.findAll().contains(USER1));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    @Ignore
    public void clear() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        @NotNull final List<UserDto> userList = createUserList();
        userList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(userList.size(), repository.findAll().size());
        entityManager.getTransaction().begin();
        repository.clear();
        entityManager.getTransaction().commit();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void existById() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.existById(USER1.getId()));
        Assert.assertFalse(repository.existById(USER2.getId()));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findAll() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        @NotNull final List<UserDto> user1UserList = createUserList();
        user1UserList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.findAll().containsAll(user1UserList));
        entityManager.getTransaction().begin();
        user1UserList.forEach(repository::remove);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findOneById() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
        Assert.assertNull(repository.findOneById(USER2.getId()));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void getSize() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        @NotNull final List<UserDto> userList = repository.findAll();
        Assert.assertEquals(userList.size(), repository.getSize().intValue());
        repository.add(NEW_USER);
        entityManager.getTransaction().commit();
        Assert.assertEquals((userList.size() + 1), repository.getSize().intValue());
        entityManager.getTransaction().begin();
        repository.remove(NEW_USER);
        entityManager.getTransaction().commit();
    }

    @Test
    public void remove() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        repository.add(NEW_USER);
        entityManager.getTransaction().commit();
        Assert.assertEquals(NEW_USER, repository.findOneById(NEW_USER.getId()));
        @NotNull final Long size = repository.getSize();
        entityManager.getTransaction().begin();
        repository.remove(NEW_USER);
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
        Assert.assertEquals(size - 1, repository.getSize().intValue());
    }

    @Test
    public void removeById() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        repository.add(NEW_USER);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(repository.findOneById(NEW_USER.getId()));
        entityManager.getTransaction().begin();
        repository.removeById(NEW_USER.getId());
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
    }

    @Test
    public void findByLogin() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertEquals(USER1, repository.findByLogin(USER1.getLogin()));
        Assert.assertNull(repository.findOneById(USER2.getLogin()));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findByEmail() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertEquals(USER1, repository.findByEmail(USER1.getEmail()));
        Assert.assertNull(repository.findByEmail("user_2@email.ru"));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void isLoginExist() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(repository.isLoginExist(USER2.getLogin()));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

    @Test
    public void isEmailExist() {
        @NotNull final UserDtoRepository repository = new UserDtoRepository(entityManager);
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(repository.isEmailExist("user_2@email.ru"));
        entityManager.getTransaction().begin();
        repository.remove(USER1);
        entityManager.getTransaction().commit();
    }

}
