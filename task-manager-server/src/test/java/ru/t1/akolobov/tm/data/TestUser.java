package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public final class TestUser {

    public static final UserDto USER1 = createUser("TEST_USER_1");
    public static final UserDto USER2 = createUser("TEST_USER_2");
    public static final UserDto NEW_USER = createUser("TEST_NEW_USER");
    public static final String USER_EMPTY_ID = "";
    public static final String USER1_ID = USER1.getId();
    public static final String USER2_ID = USER2.getId();

    @NotNull
    public static UserDto createUser(@NotNull String login) {
        @NotNull UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(login, 10, "secret"));
        user.setEmail(login + "@email.ru");
        return user;
    }

    @NotNull
    public static List<UserDto> createUserList() {
        @NotNull List<UserDto> userList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull UserDto user = new UserDto();
            @NotNull String login = "TEST_USER_" + i;
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(login, 10, "secret"));
            user.setEmail(login + "@email.ru");
            userList.add(user);
        }
        return userList;
    }

}
