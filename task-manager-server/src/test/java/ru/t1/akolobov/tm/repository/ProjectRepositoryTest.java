package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.akolobov.tm.repository.dto.UserDtoRepository;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProject;
import static ru.t1.akolobov.tm.data.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    private static EntityManager entityManager;

    @BeforeClass
    public static void prepareConnection() {
        entityManager = new ConnectionService(new PropertyService()).getEntityManager();
        @NotNull final UserDtoRepository userRepository =
                new UserDtoRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        entityManager.getTransaction().commit();
    }

    @AfterClass
    public static void closeConnection() {
        @NotNull final UserDtoRepository userRepository =
                new UserDtoRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Before
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @After
    public void clearData() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
        entityManager.getTransaction().commit();
    }

    @Test
    public void add() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository(entityManager);
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.findAll(USER1_ID).contains(project));
        Assert.assertEquals(1, repository.findAll(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository(entityManager);
        @NotNull final List<ProjectDto> projectList = createProjectList(USER1_ID);
        projectList.forEach(repository::add);
        entityManager.getTransaction().commit();
        long size = repository.getSize();
        entityManager.getTransaction().begin();
        repository.clear(USER1_ID);
        entityManager.getTransaction().commit();
        Assert.assertEquals(
                size - projectList.size(),
                repository.getSize().intValue()
        );
    }

    @Test
    public void existById() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository(entityManager);
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.existById(USER1_ID, project.getId()));
        Assert.assertFalse(repository.existById(USER2_ID, project.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository(entityManager);
        @NotNull final List<ProjectDto> user1ProjectList = createProjectList(USER1_ID);
        @NotNull final List<ProjectDto> user2ProjectList = createProjectList(USER2_ID);
        user1ProjectList.forEach(repository::add);
        user2ProjectList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(user1ProjectList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2ProjectList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final ProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        entityManager.getTransaction().commit();
        @NotNull final String projectId = project.getId();
        Assert.assertEquals(project, repository.findOneById(USER1_ID, projectId));
        Assert.assertNull(repository.findOneById(USER2_ID, projectId));
    }

    @Test
    public void getSize() {
        @NotNull final ProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
        @NotNull final List<ProjectDto> projectList = createProjectList(USER1_ID);
        projectList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(projectList.size(), repository.getSize(USER1_ID).intValue());
        entityManager.getTransaction().begin();
        repository.add(createProject(USER1_ID));
        entityManager.getTransaction().commit();
        Assert.assertEquals((projectList.size() + 1), repository.getSize(USER1_ID).intValue());
    }

    @Test
    public void remove() {
        @NotNull final ProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
        createProjectList(USER1_ID).forEach(repository::add);
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        entityManager.getTransaction().commit();
        Assert.assertEquals(project, repository.findOneById(USER1_ID, project.getId()));
        entityManager.getTransaction().begin();
        repository.remove(project);
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
    }

    @Test
    public void removeById() {
        @NotNull final ProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
        createProjectList(USER1_ID).forEach(repository::add);
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        repository.removeById(USER1_ID, project.getId());
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
    }

}
