package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.akolobov.tm.repository.dto.TaskDtoRepository;
import ru.t1.akolobov.tm.repository.dto.UserDtoRepository;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestTask.createTask;
import static ru.t1.akolobov.tm.data.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    private static EntityManager entityManager;

    @BeforeClass
    public static void prepareConnection() {
        entityManager = new ConnectionService(new PropertyService()).getEntityManager();
        @NotNull final UserDtoRepository userRepository = new UserDtoRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        entityManager.getTransaction().commit();
    }

    @AfterClass
    public static void closeConnection() {
        @NotNull final UserDtoRepository userRepository = new UserDtoRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Before
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @After
    public void clearData() {
        @NotNull final TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
        entityManager.getTransaction().commit();
    }

    @Test
    public void add() {
        @NotNull final TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.add(task);
        entityManager.getTransaction().commit();
        Assert.assertEquals(task, repository.findAll(USER1_ID).get(0));
        Assert.assertEquals(1, repository.findAll(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull final TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        @NotNull final List<TaskDto> TaskList = createTaskList(USER1_ID);
        TaskList.forEach(repository::add);
        entityManager.getTransaction().commit();
        long size = repository.getSize();
        entityManager.getTransaction().begin();
        repository.clear(USER1_ID);
        entityManager.getTransaction().commit();
        Assert.assertEquals(
                size - TaskList.size(),
                repository.getSize().intValue()
        );
    }

    @Test
    public void existById() {
        @NotNull final TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.add(task);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.existById(USER1_ID, task.getId()));
        Assert.assertFalse(repository.existById(USER2_ID, task.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        @NotNull final List<TaskDto> user1TaskList = createTaskList(USER1_ID);
        @NotNull final List<TaskDto> user2TaskList = createTaskList(USER2_ID);
        user1TaskList.forEach(repository::add);
        user2TaskList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(user1TaskList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2TaskList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.add(task);
        entityManager.getTransaction().commit();
        @NotNull final String taskId = task.getId();
        Assert.assertEquals(task, repository.findOneById(USER1_ID, taskId));
        Assert.assertNull(repository.findOneById(USER2_ID, taskId));
    }

    @Test
    public void getSize() {
        @NotNull TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        @NotNull final List<TaskDto> taskList = createTaskList(USER1_ID);
        taskList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(taskList.size(), repository.getSize(USER1_ID).intValue());
        entityManager.getTransaction().begin();
        repository.add(createTask(USER1_ID));
        entityManager.getTransaction().commit();
        Assert.assertEquals((taskList.size() + 1), repository.getSize(USER1_ID).intValue());
    }

    @Test
    public void remove() {
        @NotNull final TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        createTaskList(USER1_ID).forEach(repository::add);
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.add(task);
        entityManager.getTransaction().commit();
        Assert.assertEquals(task, repository.findOneById(USER1_ID, task.getId()));
        entityManager.getTransaction().begin();
        repository.remove(task);
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
    }

    @Test
    public void removeById() {
        @NotNull final TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        createTaskList(USER1_ID).forEach(repository::add);
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.add(task);
        repository.removeById(USER1_ID, task.getId());
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final TaskDtoRepository repository = new TaskDtoRepository(entityManager);
        @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final ProjectDto project = new ProjectDto("test-project-for-task");
        project.setUserId(USER1_ID);
        projectRepository.add(project);
        entityManager.getTransaction().commit();
        @NotNull final String projectId = project.getId();
        @NotNull final List<TaskDto> taskList = createTaskList(USER1_ID);
        taskList.forEach(t -> t.setProjectId(projectId));
        entityManager.getTransaction().begin();
        taskList.forEach(repository::add);
        repository.add(createTask(USER1_ID));
        entityManager.getTransaction().commit();
        Assert.assertEquals(taskList, repository.findAllByProjectId(USER1_ID, projectId));
        entityManager.getTransaction().begin();
        taskList.forEach(repository::remove);
        projectRepository.removeById(USER1_ID, projectId);
        entityManager.getTransaction().commit();
    }

}
