package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.ProjectDto;

import java.util.ArrayList;
import java.util.List;

public final class TestProject {

    @NotNull
    public static ProjectDto createProject() {
        return new ProjectDto("new-project", "new-project-desc");
    }

    @NotNull
    public static ProjectDto createProject(@NotNull final String userId) {
        @NotNull ProjectDto project = createProject();
        project.setUserId(userId);
        return project;
    }

    @NotNull
    public static List<ProjectDto> createProjectList(@NotNull final String userId) {
        @NotNull List<ProjectDto> projectList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            @NotNull ProjectDto project = new ProjectDto("project-" + i, "project-" + i + "desc");
            project.setUserId(userId);
            projectList.add(project);
        }
        return projectList;
    }

}
