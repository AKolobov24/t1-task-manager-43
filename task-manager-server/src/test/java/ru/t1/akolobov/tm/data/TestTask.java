package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.TaskDto;

import java.util.ArrayList;
import java.util.List;

public final class TestTask {

    @NotNull
    public static TaskDto createTask() {
        return new TaskDto("task-1", "task-1-desc");
    }

    @NotNull
    public static TaskDto createTask(@NotNull final String userId) {
        @NotNull TaskDto task = createTask();
        task.setUserId(userId);
        return task;
    }

    @NotNull
    public static List<TaskDto> createTaskList(@NotNull final String userId) {
        @NotNull List<TaskDto> taskList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            @NotNull TaskDto task = new TaskDto("task-" + i, "task-" + i + "-desc");
            task.setUserId(userId);
            taskList.add(task);
        }
        return taskList;
    }

}
