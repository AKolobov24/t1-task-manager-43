package ru.t1.akolobov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.akolobov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.akolobov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.akolobov.tm.repository.dto.TaskDtoRepository;
import ru.t1.akolobov.tm.repository.dto.UserDtoRepository;
import ru.t1.akolobov.tm.service.dto.ProjectTaskDtoService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private final static PropertyService propertyService = new PropertyService();
    @NotNull
    private final static IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final static EntityManager repositoryEntityManager = connectionService.getEntityManager();
    @NotNull
    private final static IUserDtoRepository userRepository = new UserDtoRepository(repositoryEntityManager);
    @NotNull
    private final ITaskDtoRepository taskRepository = new TaskDtoRepository(repositoryEntityManager);
    @NotNull
    private final IProjectDtoRepository projectRepository = new ProjectDtoRepository(repositoryEntityManager);
    @NotNull
    private final IProjectTaskDtoService service = new ProjectTaskDtoService(connectionService);

    @BeforeClass
    public static void addUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        repositoryEntityManager.getTransaction().commit();
    }

    @AfterClass
    public static void clearUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        repositoryEntityManager.getTransaction().commit();
        repositoryEntityManager.close();
    }

    @Before
    public void initRepository() {
        repositoryEntityManager.getTransaction().begin();
        createTaskList(USER1_ID).forEach(taskRepository::add);
        createProjectList(USER1_ID).forEach(projectRepository::add);
        repositoryEntityManager.getTransaction().commit();
    }

    @After
    public void clearRepository() {
        repositoryEntityManager.getTransaction().begin();
        taskRepository.clear(USER1_ID);
        projectRepository.clear(USER1_ID);
        taskRepository.clear(USER2_ID);
        projectRepository.clear(USER2_ID);
        repositoryEntityManager.getTransaction().commit();
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final ProjectDto project = projectRepository.findAll(USER1_ID).get(0);
        @NotNull final TaskDto task = taskRepository.findAll(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        repositoryEntityManager.refresh(task);
        final TaskDto bindTask = taskRepository.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertEquals(project.getId(), bindTask.getProjectId());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.bindTaskToProject(USER_EMPTY_ID, project.getId(), task.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), project.getId())
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, task.getId(), project.getId())
        );
    }

    @Test
    public void removeProjectById() {
        @NotNull final ProjectDto project = projectRepository.findAll(USER1_ID).get(0);
        @NotNull final List<TaskDto> taskList = taskRepository.findAll(USER1_ID);
        service.bindTaskToProject(USER1_ID, project.getId(), taskList.get(0).getId());
        service.bindTaskToProject(USER1_ID, project.getId(), taskList.get(1).getId());

        service.removeProjectById(USER1_ID, project.getId());
        Assert.assertEquals(taskList.size() - 2, taskRepository.findAll(USER1_ID).size());
        Assert.assertFalse(taskRepository.findAll(USER1_ID).contains(taskList.get(0)));
        Assert.assertFalse(taskRepository.findAll(USER1_ID).contains(taskList.get(1)));

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.removeProjectById(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.removeProjectById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final ProjectDto project = projectRepository.findAll(USER1_ID).get(0);
        @NotNull final TaskDto task = taskRepository.findAll(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        repositoryEntityManager.refresh(task);
        TaskDto bindTask = taskRepository.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertEquals(project.getId(), bindTask.getProjectId());

        service.unbindTaskFromProject(USER1_ID, task.getId());
        repositoryEntityManager.refresh(bindTask);
        bindTask = taskRepository.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertNotEquals(project.getId(), bindTask.getProjectId());
        Assert.assertNull(bindTask.getProjectId());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER1_ID, USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.unbindTaskFromProject(USER1_ID, project.getId())
        );
    }

}
