package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.dto.model.SessionDto;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.repository.dto.SessionDtoRepository;
import ru.t1.akolobov.tm.repository.dto.UserDtoRepository;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestSession.createSession;
import static ru.t1.akolobov.tm.data.TestSession.createSessionList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    private static EntityManager entityManager;

    @BeforeClass
    public static void prepareConnection() {
        entityManager = new ConnectionService(new PropertyService()).getEntityManager();
        @NotNull final UserDtoRepository userRepository = new UserDtoRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        entityManager.getTransaction().commit();
    }

    @AfterClass
    public static void closeConnection() {
        @NotNull final UserDtoRepository userRepository = new UserDtoRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Before
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @After
    public void clearData() {
        @NotNull final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
        entityManager.getTransaction().commit();
    }

    @Test
    public void add() {
        @NotNull final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.add(session);
        entityManager.getTransaction().commit();
        Assert.assertEquals(session, repository.findAll().get(0));
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void clear() {
        @NotNull SessionDtoRepository repository = new SessionDtoRepository(entityManager);
        @NotNull final List<SessionDto> sessionList = createSessionList(USER1_ID);
        sessionList.forEach(repository::add);
        @NotNull final SessionDto user2Session = createSession(USER2_ID);
        repository.add(user2Session);
        entityManager.getTransaction().commit();
        Assert.assertEquals(sessionList.size() + 1, repository.findAll().size());
        entityManager.getTransaction().begin();
        repository.clear(USER1_ID);
        entityManager.getTransaction().commit();
        Assert.assertEquals(1, repository.findAll().size());
        Assert.assertEquals(user2Session, repository.findAll().get(0));
    }

    @Test
    public void existById() {
        @NotNull final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.add(session);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.existById(USER1_ID, session.getId()));
        Assert.assertFalse(repository.existById(USER2_ID, session.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
        @NotNull final List<SessionDto> user1SessionList = createSessionList(USER1_ID);
        @NotNull final List<SessionDto> user2SessionList = createSessionList(USER2_ID);
        user1SessionList.forEach(repository::add);
        user2SessionList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(user1SessionList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2SessionList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.add(session);
        entityManager.getTransaction().commit();
        @NotNull final String sessionId = session.getId();
        Assert.assertEquals(session, repository.findOneById(USER1_ID, sessionId));
        Assert.assertNull(repository.findOneById(USER2_ID, sessionId));
    }

    @Test
    public void getSize() {
        @NotNull final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
        @NotNull final List<SessionDto> sessionList = createSessionList(USER1_ID);
        sessionList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(sessionList.size(), repository.getSize(USER1_ID).intValue());
        entityManager.getTransaction().begin();
        repository.add(createSession(USER1_ID));
        entityManager.getTransaction().commit();
        Assert.assertEquals((sessionList.size() + 1), repository.getSize(USER1_ID).intValue());
    }

    @Test
    public void remove() {
        @NotNull final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
        createSessionList(USER1_ID).forEach(repository::add);
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.add(session);
        entityManager.getTransaction().commit();
        Assert.assertEquals(session, repository.findOneById(USER1_ID, session.getId()));
        entityManager.getTransaction().begin();
        repository.remove(session);
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, session.getId()));
    }

    @Test
    public void removeById() {
        @NotNull final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
        createSessionList(USER1_ID).forEach(repository::add);
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.add(session);
        repository.removeById(USER1_ID, session.getId());
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, session.getId()));
    }

}
