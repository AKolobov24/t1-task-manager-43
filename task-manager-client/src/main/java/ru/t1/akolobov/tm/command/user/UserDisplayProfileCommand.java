package ru.t1.akolobov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.dto.request.UserProfileRequest;
import ru.t1.akolobov.tm.exception.user.UserNotFoundException;

public final class UserDisplayProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-display-profile";

    @NotNull
    public static final String DESCRIPTION = "Display current user profile.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY USER PROFILE]");
        @Nullable final UserDto user = getAuthEndpoint().profile(new UserProfileRequest(getToken())).getUser();
        if (user == null) throw new UserNotFoundException();
        displayUser(user);
    }

    @Override
    protected void displayUser(@Nullable final UserDto user) {
        super.displayUser(user);
        if (user == null) throw new UserNotFoundException();
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
    }

}
